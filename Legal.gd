extends Control

var expat_license = """
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the \"Software\"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

var credits = [
	"""Godot Engine
	
	Copyright (c) 2007-2020 Juan Linietsky, Ariel Manzur.
	Copyright (c) 2014-2020 Godot Engine contributors (cf. AUTHORS.md).
	""" + expat_license,
	"""GodotAdMob
	
	MIT License
	
	Copyright (c) 2019 Shin-NiL
	""" + expat_license,
	"""
	Clear Day 
	by Benjamin Tissot 
	
	https://www.bensound.com/royalty-free-music/track/clear-day
	""",
	"""
	Small Dog Barking Sound
	(http://soundbible.com/2174-Small-Dog-Barking.html)
	by Daniel Simion
	
	CC BY 3.0
	https://creativecommons.org/licenses/by/3.0/
	""",
	"""
	Button On Sound
	(http://soundbible.com/1294-Button-Click-Off.html)
	Button Click On Sound
	(http://soundbible.com/1280-Click-On.html)
	both by Mike Koenig
	
	CC BY 3.0:
	https://creativecommons.org/licenses/by/3.0/
	""",
	"""
	Eating Sound
	(http://soundbible.com/976-Eating.html)
	by Caroline Ford
	
	CC BY 3.0:
	https://creativecommons.org/licenses/by/3.0/
	""",
	"""
	Dogs Howling Sound
	(http://soundbible.com/2149-Dogs-Howling.html)
	Dog Panting Fast Sound
	(http://soundbible.com/2147-Dog-Panting-Fast.html)
	both by Daniel Simon
	
	CC BY 3.0:
	https://creativecommons.org/licenses/by/3.0/
	""",
	"""
	Sunny Day Sounds
	(http://soundbible.com/1661-Sunny-Day.html) 
	by stephan
	
	public domain.
	""",
	"""
	Common names are retrieved from:
	
	https://www2.census.gov/topics/genealogy/1990surnames/dist.male.first
	https://www2.census.gov/topics/genealogy/1990surnames/dist.female.first
	
	public domain.
	"""
]

