extends Area2D

var follow_mouse = false
var types_of_food

func _ready():
	var rng = RandomNumberGenerator.new()
	rng.randomize()
	var rand_num = rng.randi_range(1, 6)
	if rand_num == 1:
		types_of_food = 'healthy'
		$food1.show()
		$food2.hide()
		$food3.hide()
		$food4.hide()
		$chocolate.hide()
		$grapes.hide()
	elif rand_num == 2:
		types_of_food = 'healthy'
		$food1.hide()
		$food2.show()
		$food3.hide()
		$food4.hide()
		$chocolate.hide()
		$grapes.hide()
	elif rand_num == 3:
		types_of_food = 'healthy'
		$food1.hide()
		$food2.hide()
		$food3.show()
		$food4.hide()
		$chocolate.hide()
		$grapes.hide()
	elif rand_num == 4:
		types_of_food = 'healthy'
		$food1.hide()
		$food2.hide()
		$food3.hide()
		$food4.show()
		$chocolate.hide()
		$grapes.hide()
	elif rand_num == 5:
		types_of_food = 'harmful'
		$food1.hide()
		$food2.hide()
		$food3.hide()
		$food4.hide()
		$chocolate.show()
		$grapes.hide()
	elif rand_num == 6:
		types_of_food = 'harmful'
		$food1.hide()
		$food2.hide()
		$food3.hide()
		$food4.hide()
		$chocolate.hide()
		$grapes.show()
#	var w = OS.get_unix_time() % 14
#	var h = OS.get_unix_time() % 7
#	get_node("Sprite").region_enabled = true
#	var unit_h = 272/8
#	var unit_w = 510/15
#	get_node("Sprite").set_region_rect(
#		Rect2(unit_w*w, unit_h*h, unit_w, unit_h))


func _physics_process(delta):
	if follow_mouse:
		set_position(get_global_mouse_position())


func _on_MouseInteract_button_down():
	follow_mouse = true


func _on_MouseInteract_button_up():
	follow_mouse = false


func _on_Food_body_entered(body):
	if body.has_method("eat_food_healthy"):
		if types_of_food == "healthy":
			body.eat_food_healthy()
			self.queue_free()
		elif types_of_food == "harmful":
			body.eat_food_harmful()
			self.queue_free()
