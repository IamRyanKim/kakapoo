extends Node2D

enum SOUNDS {
	eating,
	barking,
	howling,
	panting
}
var curr_sound = null


func get_sound(sound_type):
	if sound_type == SOUNDS.eating:
		return $Eating
	elif sound_type == SOUNDS.barking:
		return $Barking1
	elif sound_type == SOUNDS.howling:
		return $Howling
	elif sound_type == SOUNDS.panting:
		return $Panting


func stop(sound_type):
	var sound = get_sound(sound_type)
	sound.stop()


func play(sound_type):
	var next_sound = get_sound(sound_type)
	if (curr_sound != null and 
		curr_sound == next_sound and curr_sound.playing):
		return
	curr_sound = next_sound
	curr_sound.play()

