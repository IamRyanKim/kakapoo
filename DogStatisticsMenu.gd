extends Control

var dog = null
var follow_mouse = false
var prev_mouse_pos = Vector2.ZERO


func _process(delta):
	if follow_mouse:
		var curr_mouse_pos = get_global_mouse_position() 
		var mouse_delta = curr_mouse_pos - prev_mouse_pos
		set_position(get_position() + mouse_delta)
		prev_mouse_pos = curr_mouse_pos
	if weakref(dog).get_ref(): # Check if dog hasn't been removed
		get_node("VBoxContainer/HealthLabel").set_text("Health: " + dog.healthiness)
		get_node("VBoxContainer/AgeLabel").set_text("Age: " + dog.age_status)
	else:
		queue_free()


func set_dog(dog):
	self.dog = dog
	get_node("VBoxContainer/DogNameEdit").set_text(dog.dog_name)
	get_node("VBoxContainer/HBoxContainer/GenderLabel").set_text("Gender: " + dog.gender)

func _on_OKButton_button_down():
	queue_free()


func _on_DogNameEdit_text_changed(new_text):
	dog.set_name(new_text)


func _on_TextureButton_button_down():
	prev_mouse_pos = get_global_mouse_position()
	follow_mouse = true


func _on_TextureButton_button_up():
	follow_mouse = false


func _on_SellButton_button_down():
	dog.sell()
