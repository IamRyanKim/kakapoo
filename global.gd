extends Node

var loader
var wait_frames
var time_max = 100 # msec
var current_scene
var current_scene1
var loading_scene
var loading
var resource

func _ready():
	var root = get_tree().get_root()
	current_scene = root.get_child(root.get_child_count() -1)
	loading_scene = ResourceLoader.load("res://loading.tscn")

func goto_scene(path): # Game requests to switch to this scene.
	loader = ResourceLoader.load_interactive(path)
	if loader == null:
#		show_error()
		return
	set_process(true)
	if current_scene.get_name() == "splash":
		current_scene.queue_free() # Get rid of the old scene.
		loading = loading_scene.instance()
		get_node("/root").add_child(loading)
	get_node("/root/loading/AnimationPlayer").play("loading")

	# Start your "loading..." animation.
#	get_node("/root/AnimationPlayer").play("loading")

	wait_frames = 1

func _process(time):
	if loader == null:
		# no need to process anymore
		set_process(false)
		return

	# Wait for frames to let the "loading" animation show up.
	if wait_frames > 0:
		wait_frames -= 1
		return

	var t = OS.get_ticks_msec()
	# Use "time_max" to control for how long we block this thread.
	while OS.get_ticks_msec() < t + time_max:
		# Poll your loader.
		var err = loader.poll()

		if err == ERR_FILE_EOF: # Finished loading.
			var resource = loader.get_resource()
			loader = null
			set_new_scene(resource)
			break
		elif err == OK:
			loading.get_node("AnimationPlayer").play("loading")
		else: # Error during loading.
#			show_error()
			loader = null
			break
			
#func update_progress():
#	loading.get_node("AnimationPlayer").play("loading")

func set_new_scene(scene_resource):
	current_scene = scene_resource.instance()
	get_node("/root").add_child(current_scene)

#var current_scene = null
#
#func _ready():
#	var root = get_tree().get_root()
#	current_scene = root.get_child(root.get_child_count() - 1)
#
#func goto_scene(path):
#	# This function will usually be called from a signal callback,
#	# or some other function in the current scene.
#	# Deleting the current scene at this point is
#	# a bad idea, because it may still be executing code.
#	# This will result in a crash or unexpected behavior.
#
#	# The solution is to defer the load to a later time, when
#	# we can be sure that no code from the current scene is running:
#
#	call_deferred("_deferred_goto_scene", path)
#
#
#func _deferred_goto_scene(path):
#	# It is now safe to remove the current scene
#	current_scene.free()
#
#	# Load the new scene.
#	var s = ResourceLoader.load(path)
#
#	# Instance the new scene.
#	current_scene = s.instance()
#
#	# Add it to the active scene, as child of root.
#	get_tree().get_root().add_child(current_scene)
#
#	# Optionally, to make it compatible with the SceneTree.change_scene() API.
#	get_tree().set_current_scene(current_scene)
