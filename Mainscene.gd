extends Node2D

var cash = 5000
var time_elapsed = 0
var dog_names = preload("res://Names.tscn").instance()

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
func _physics_process(delta):
	$background/AnimationPlayer.play("background")
	time_elapsed += delta
	var health = calculate_health()
	if ($SunnyDaySound.get_playback_position() > 20.5):
		$SunnyDaySound.play()
	get_node("CanvasLayer/Time").set_text("Time Elapsed: %d s" % int(time_elapsed))
	if int(health) > 0:
#		get_node("CanvasLayer/CashPanel/CashSprite/CashLabel").set_text("%.2f" % cash)
#		get_node("CanvasLayer/HealthSprite/HealthLabel").set_text(str(health))
		get_node("CanvasLayer/CashSprite/CashLabel").set_text("%d" % cash)

	else:
#		get_node("CanvasLayer/CashPanel/CashSprite/CashLabel").set_text("%.2f" % cash)
#		get_node("CanvasLayer/HealthSprite/HealthLabel").set_text(str(0))
		get_node("CanvasLayer/CashSprite/CashLabel").set_text("%d" % cash)


# Called when the node enters the scene tree for the first time.
func _ready():
	var save_dict = load_save()
	var Dog #= load("res://dog.tscn")
	if is_valid_save(save_dict):
		cash = save_dict['cash']
		var dog_stats = save_dict['dogs']
#		time_elapsed = save_dict['time elapsed']
#		time_elapsed += (OS.get_unix_time()
#			- save_dict['time at last save'])
		for stat in dog_stats:
			Dog = load("res://" + stat['dogtype'] + ".tscn")
			var dog = Dog.instance()
			add_child(dog)
			var x = float(stat['position'][0])
			var y = float(stat['position'][1])
			dog.set_name(stat['name'])
			dog.set_position(Vector2(x, y))
			dog.health = int(stat['health'])
			dog.set_gender(stat['gender'])
			dog.lifespan = int(stat['lifespan'])
			dog.add_to_group("dogs", true)
		var Excreta = load("res://excreta.tscn")
		for elem in save_dict["excreta"]:
			var excreta = Excreta.instance()
			var x = elem[0]
			var y = elem[1]
			excreta.set_position(Vector2(x, y))
			get_node("poop-view").add_child(excreta)
	else:
		Dog = load("res://goldenbrowndog.tscn")
		var dog = Dog.instance()
		dog.set_position(get_viewport_rect().size/2)
		dog.add_to_group("dogs", true)
		self.add_child(dog)
	var Menu = load("res://menu.tscn")
	var menu = Menu.instance()
	menu.set_position(get_viewport_rect().size/7)
	add_child(menu)
	if OS.get_name() == "Android":
		load_adds()
	get_node("SunnyDaySound").play()
		

func load_adds():
#	$AdMob.set_position().global_position(Vector2(200, 600))
	get_node("AdMob").load_banner()
#	get_node("AdMob").load_interstitial()
#	get_node("AdMob").load_rewarded_video()

func calculate_health():
	var health = 0
	var num = 0
	for dog in get_tree().get_nodes_in_group("dogs"):
		num += 1
		if num >0:
			health += dog.health
	if num > 0:
		return str((health/num))
	else:
		return 0


func _on_Timer_timeout():
	cash += 1


func _on_buydogbutton_pressed():
	get_node("PopupPanel").show()

func purchase_food(pos):
	cash -= 10
	var food = preload("res://food.tscn").instance()
	food.set_position(pos)
	add_child(food)

func purchase_dog(dogtype, pos):
	var scene
	if dogtype == 1:
		scene = preload("res://goldenbrowndog.tscn")
	elif dogtype == 2:
		scene = preload("res://apricot_poodle.tscn")
	else:
		scene = preload("res://white_bichon.tscn")

	var dog = scene.instance()
	dog.set_position(pos)
	dog.add_to_group("dogs", true)
	add_child(dog)


func save_exists():
	var check_file = File.new()
	if OS.get_name() == "Android":
		if check_file.file_exists("user://game.save"):
			check_file.close()
			return true
		else:
			check_file.close()
			return false
	else:
		if check_file.file_exists("res://game.save"):
			check_file.close()
			return true
		else:
			check_file.close()
			return false

func is_valid_save(save_dict):
	if not save_dict:
		return false
	var save_format = {
		"time at last save": 0,
		"time elapsed": 0,
		"cash": 0,
		"dogs": [],
		"excreta": []
	}
	var dog_format = {
		"name": "Dog",
		"position": [0.0, 0.0],
		"health": 100,
		"gender": "M",
		"dogtype": "type",
		"lifespan": 100
	}
	for key in save_format:
		if not save_dict.has(key):
			return false
	var dogs_list = save_dict["dogs"]
	if len(dogs_list) == 0:
		return true
	else:
		var dog_dict = dogs_list[0]
		if typeof(dog_dict) != TYPE_DICTIONARY:
			return false
		for key in dog_format:
			if not dog_dict.has(key):
				return false
	return true


func load_save():
	var save_game = File.new()
	var save_dict = null
	if OS.get_name() == "Android":
		if save_game.file_exists("user://game.save"):
			save_game.open("user://game.save", File.READ)
			save_dict = parse_json(save_game.get_line())
	else:
		if save_game.file_exists("res://game.save"):
			save_game.open("res://game.save", File.READ)
			save_dict = parse_json(save_game.get_line())
	save_game.close()
	return save_dict


func save_game():
	var time_at_last_save = OS.get_unix_time()
	var save_game = File.new()
	var dogs = []
	var excreta = []
	for dog in get_tree().get_nodes_in_group("dogs"):
		var dog_stat = {}
		dog_stat['name'] = dog.dog_name
		dog_stat['position'] = [dog.get_position().x, 
								dog.get_position().y]
		dog_stat['health'] = dog.health
		dog_stat['gender'] = dog.gender
		dog_stat['dogtype'] = dog.dogtype
		dog_stat['lifespan'] = dog.lifespan
		dogs.append(dog_stat)
	for ex in get_node("poop-view").get_children():
		var pos = ex.get_position()
		excreta.append([pos.x, pos.y])
	if OS.get_name() == "Android":
		save_game.open("user://game.save", File.WRITE)
	else:
		save_game.open("res://game.save", File.WRITE)
	save_game.store_line(to_json({
		"time at last save": time_at_last_save,
		"time elapsed": time_elapsed,
		"cash": cash,
		"dogs": dogs,
		"excreta": excreta
	}))
	

# https://docs.godotengine.org/en/stable/
# tutorials/misc/handling_quit_requests.html
func _notification(what): #i just commented for testing purpose
	if what == MainLoop.NOTIFICATION_WM_QUIT_REQUEST:
		self.save_game()
		get_tree().quit()


func _on_goldenbrown_pressed():
	get_node("PopupPanel").hide()
	get_node("goldenbrownpopup").show()


func _on_gbyes_pressed(): #1 is goldenbrown, 2 is white, 3 is brown
	cash -= 5000
	purchase_dog(1, Vector2(250, 250))
	get_node("goldenbrownpopup").hide()


func _on_gbno_pressed():
	get_node("goldenbrownpopup").hide()


func _on_touch_to_cash_pressed():
	cash += 5
