extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	get_node("SunnyDaySound").play()


func _process(delta):
	$AnimationPlayer.play("splash")
	if $SunnyDaySound.get_playback_position() > 20.5:
		$SunnyDaySound.play()


func _on_pushtostart_pressed():
	Global.goto_scene("res://Mainscene.tscn")
