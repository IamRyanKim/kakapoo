extends Area2D


var follow_mouse = false
var bodies = {}

func _physics_process(delta):
	if follow_mouse:
		set_position(get_global_mouse_position())
	if OS.get_ticks_msec() % 139 == 0:
		for body in bodies.keys():
			if not weakref(bodies[body]).get_ref():
				bodies.erase(body.get_name())
			else:
				bodies[body].add_health(-1)
				# print(bodies[body].health)
				


func _on_Excreta_body_entered(body):
	if body.has_method("add_health"):
		bodies[body.get_name()] = body


func _on_MouseInteract_button_up():
	follow_mouse = not follow_mouse


func _on_MouseInteract_button_down():
	follow_mouse = not follow_mouse


func _on_Excreta_body_exited(body):
	if bodies.has(body.get_name()):
		bodies.erase(body.get_name())
