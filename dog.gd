extends KinematicBody2D

var dog_name = "Dog"
var text_label = null
var doing_something = false
var dogtype
var age_status
var healthiness
var MAX_LIFESPAN = 788400 #6month is 15768000 but not supersure about the accuracy
var lifespan = MAX_LIFESPAN
var MAX_HEALTH = 100
var health = MAX_HEALTH
var bark_interval = 5.0
# Animation variables
var other_animation_playing = false
# Random number generator
var rng = RandomNumberGenerator.new()

# Movement variables
export var speed = 25
var direction : Vector2
var last_direction = Vector2(0, 1)
var bounce_countdown = 0
var follow_mouse = false
var prev_mouse_pos = Vector2.ZERO
var mouse_pressed_time = 0.0
var mouse_pressed_dist = 0.0

# reproduction variables
var gender = "M"
var is_seeking_mate = false
var mates = []
var refractory = 3.0
var Offspring = load("res://goldenbrowndog.tscn")
var Excreta = load("res://excreta.tscn")

# variables that involve the digestive system
var bm_countdown = 10000.0

# Called when the node enters the scene tree for the first time.
func _ready():
	rng.randomize()
	var gender_val = rng.randf()
	if gender_val > 0.45 and gender_val <= 0.9:
		gender = "F"
	elif gender_val > 0.9:
		gender = "T"
	rng.randomize()
	bark_interval = rng.randf()*bark_interval
	set_random_name(gender)
	$deadanimation.connect("animation_finished", self, 
						   "_on_deadanimation_animation_finished")
		
		
func set_random_name(gender):
	rng.randomize()
	var name_val = rng.randf()
	var dog_names = get_parent().dog_names
	var names = dog_names.MALE_NAMES if gender == "M" else \
				dog_names.FEMALE_NAMES
	set_name(names[int(name_val*(len(names) - 1))])


func set_name(dog_name):
	self.dog_name = dog_name
	$CenterContainer/name.set_text(dog_name)


func _on_Timer_timeout():
	
	if is_seeking_mate:
		return

	if bounce_countdown == 0:
		var random_number = rng.randf()
		if random_number < 0.05:
			direction = Vector2.ZERO
		elif random_number < 0.1:
			direction = Vector2.DOWN.rotated(rng.randf() * 2 * PI)
			
	
	# Update bounce countdown
	if bounce_countdown > 0:
		bounce_countdown = bounce_countdown - 1
	
func _physics_process(delta):
#	print("lifespan is" + str(lifespan))
#	print("healthiness is" + str(health))
	if lifespan < 394200:
		age_status = "old"
	else:
		age_status = "regular"
	
	if health < 50:
		$DogSounds.play($DogSounds.SOUNDS.howling)
		healthiness = "sick"
	else:
		$DogSounds.stop($DogSounds.SOUNDS.howling)
		healthiness = "healthy"
		if bark_interval < 0.0:
			rng.randomize()
			bark_interval = 10.0*rng.randf()
			$DogSounds.play($DogSounds.SOUNDS.barking)
			
	
	if doing_something:
		speed = 0
	else:
		speed = 25

	if follow_mouse:
		mouse_pressed_time += delta
		var curr_mouse_pos = get_global_mouse_position()
		var mouse_delta = curr_mouse_pos - prev_mouse_pos
		set_position(curr_mouse_pos)
		mouse_pressed_dist += mouse_delta.length()
		prev_mouse_pos = curr_mouse_pos
	lifespan -= delta
	health -= delta
	refractory -= delta
	if (gender == 'M' and mates != [] and refractory <= 0.0
		 and not is_seeking_mate and OS.get_ticks_msec() % 100000 == 0):
		is_seeking_mate = true 
	if is_seeking_mate:
		var mate = mates[-1]
		if not weakref(mate).get_ref():
			is_seeking_mate = false
			mates.pop_back()
		else:
			direction = (mate.position - 
				self.position).normalized()
			speed = 50
	var movement = direction * speed * delta
	move_and_slide(movement)
	bm_countdown -= delta
	bark_interval -= delta
	if bm_countdown < 0:
		var excreta = Excreta.instance()
		excreta.set_position(get_position())
		get_parent().get_node("poop-view").add_child(excreta)
		bm_countdown = 10000.0
	if health <= 0 or lifespan <= 0:
		dead()
	var collision = move_and_collide(movement)
	
	if collision != null:
		if is_seeking_mate:
			var dog
			var mate = mates[-1]
			rng.randomize()
			var dog_type_val = rng.randf()
			if dog_type_val <= 0.5:
				Offspring = load("res://%s.tscn" % self.dogtype)
			else:
				Offspring = load("res://%s.tscn" % mate.dogtype)
			dog = Offspring.instance()
			dog.set_position(self.position + Vector2(100.0, 100.0))
			is_seeking_mate = false
			dog.add_to_group("dogs", true)
			get_parent().add_child(dog)
			speed = 25
			refractory = 3.0
		direction = direction.rotated(rng.randf_range(PI/4, PI/2))
		bounce_countdown = rng.randi_range(2, 5)
	
	# Animate skeleton based on direction
	if not other_animation_playing:
		animates_monster(direction)

func get_animation_direction(direction: Vector2):
	var norm_direction = direction.normalized()
	if norm_direction.y >= 0.707:
		return "down"
	elif norm_direction.y <= -0.707:
		return "up"
	elif norm_direction.x <= -0.707:
		return "left"
	elif norm_direction.x >= 0.707:
		return "right"
	return "down"

func animates_monster(direction: Vector2):
	if direction != Vector2.ZERO:
		last_direction = direction
		
		# Choose walk animation based on movement direction
		var animation
		if age_status == "regular":
			if healthiness == "healthy":
				animation = "regular_" + get_animation_direction(last_direction) + "_walk"
			elif healthiness == "sick":
				animation = "regular_sick_" + get_animation_direction(last_direction) + "_walk"
		elif age_status == "old":
			if healthiness == "healthy":
				animation = "old_" + get_animation_direction(last_direction) + "_walk"
			elif healthiness == "sick":
				animation = "old_sick_" + get_animation_direction(last_direction) + "_walk"
		
		# Play the walk animation
		$AnimatedSprite.play(animation)
	else:
		# Choose idle animation based on last movement direction and play it
		var animation
		if age_status == "regular":
			if healthiness == "healthy":
				animation = "regular_" + get_animation_direction(last_direction) + "_idle"
			elif healthiness == "sick":
				animation = "regular_sick_" + get_animation_direction(last_direction) + "_idle"
		elif age_status == "old":
			if healthiness == "healthy":
				animation = "old_" + get_animation_direction(last_direction) + "_idle"
			elif healthiness == "sick":
				animation = "old_sick_" + get_animation_direction(last_direction) + "_idle"
		$AnimatedSprite.play(animation)

func dead():
	doing_something = true
	$AnimatedSprite.hide()
	$deadanimation.show()
	if age_status == "regular":
		$deadanimation.play("regular_dead")
	elif age_status == "old":
		$deadanimation.play("old_dead")

func _on_deadanimation_animation_finished():
	queue_free()

func get_gender():
	return self.gender
	
func set_gender(gender):
	self.gender = gender
	
func add_health(health):
	if not (self.health + health) > self.MAX_HEALTH:
		self.health += health

func decrease_health(health):
	self.health -= health
	
func eat_food_healthy():
	$DogSounds.play($DogSounds.SOUNDS.eating)
	bm_countdown = 3.0
	other_animation_playing = true
	doing_something = true
	$reactiontimer.start()
	var animation
	if age_status == "regular":
		animation = "regular_" + get_animation_direction(last_direction) + "_reaction_good"
	elif age_status == "old":
		animation = "old_" + get_animation_direction(last_direction) + "_reaction_good"
	$AnimatedSprite.play(animation)
	add_health(50)

func eat_food_harmful():
	$DogSounds.play($DogSounds.SOUNDS.eating)
	bm_countdown = 3.0
	other_animation_playing = true
	doing_something = true
	$reactiontimer.start()
	var animation
#	if age_status == "regular":
#		animation = "regular_" + get_animation_direction(last_direction) + "_reaction_bad"
#	elif age_status == "old":
#		animation = "old_" + get_animation_direction(last_direction) + "_reaction_bad"
#	$AnimatedSprite.play(animation)
	$eating_harmful_anim.play("eating_harmful")
	decrease_health(30)
	
func sell():
	var health_modifier = 0.5 + 0.5*(health/MAX_HEALTH)
	var age_cost_modifier = lifespan/MAX_LIFESPAN
	get_parent().cash += int(5000*age_cost_modifier*health_modifier)
	queue_free()
	
func _on_FieldOfView_body_entered(body):
	if (body.has_method("get_gender") and 
		body.get_gender() == "F" and self.gender == "M"):
		mates.append(body)
		# $DogSounds.play($DogSounds.SOUNDS.panting)
		# is_seeking_mate = true
		# mate = body

func _on_MouseInteract_button_up():
	follow_mouse = false
	if mouse_pressed_time > 0.5 and mouse_pressed_dist < 0.1:
		init_menu()
	mouse_pressed_time = 0.0
	mouse_pressed_dist = 0.0

func _on_MouseInteract_button_down():
	prev_mouse_pos = get_global_mouse_position()
	follow_mouse = true

func _on_MouseInteract_gui_input(event):
	if (('doubleclick' in event and event.doubleclick) 
		or ('index' in event and event.index > 0)):
			init_menu()
			

func init_menu():
	var Menu = load("res://DogStatisticsMenu.tscn")
	var menu = Menu.instance()
	menu.set_dog(self)
	var pos = get_parent().get_viewport_rect().size/4
	menu.set_position(pos)
	get_parent().add_child(menu)


func _on_reactiontimer_timeout():
	other_animation_playing = false
	doing_something = false
