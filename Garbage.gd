extends Area2D


var follow_mouse = false

func _physics_process(delta):
	if follow_mouse:
		set_position(get_global_mouse_position())

func _on_MouseInteract_button_up():
	follow_mouse = not follow_mouse


func _on_MouseInteract_button_down():
	follow_mouse = not follow_mouse


func _on_Garbage_area_entered(area):
	if area.has_method("_on_Excreta_body_entered") or area.has_method("_on_Food_body_entered"):
		area.queue_free()
#	if "Excreta" in area.get_name():
#		area.queue_free()
#	elif "Food" in area.get_name():
#		area.queue_free()
