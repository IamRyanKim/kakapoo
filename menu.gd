extends TextureButton

var follow_mouse
var mouse_pressed_time
var dog_to_purchase = 1

func _process(delta):
	if follow_mouse:
		mouse_pressed_time += delta
		if mouse_pressed_time > 0.1:
			set_position(get_global_mouse_position()
						 - self.get_rect().size/2)

func _on_Close_button_down():
	get_node("CancelSound").play()
	get_node("PopupPanel").hide()


func _on_Save_Game_button_down():
	get_parent().save_game()
	get_parent().get_tree().quit()


func _on_OpenButton_button_up():
	follow_mouse = false
	if mouse_pressed_time > 0.2:
		return
#	get_node("PopupPanel").set_position(get_position()
#										+ self.get_rect().size)
	get_node("PopupPanel").set_position(Vector2(100,210))
	get_node("PopupPanel").show()


func _on_OpenButton_button_down():
	mouse_pressed_time = 0.0
	follow_mouse = true


func _on_Buy_Food_button_down():
	get_node("AcceptSound").play()
	var pos = self.get_position()
	pos += get_viewport_rect().size/10
	get_parent().purchase_food(pos)


func _on_Buy_Dog_button_down():
	get_node("AcceptSound").play()
#	get_node("BuyDogPopup").set_position(get_position()
#	 + self.get_rect().size + 
#	Vector2(0.0, get_node("PopupPanel").get_rect().size.y))
	get_node("BuyDogPopup").set_position(Vector2(120,220))
	get_node("BuyDogPopup").show()
	$PopupPanel.hide()

func buy_dog(dog_type):
	get_node("BuyDogPopup").hide()
#	get_node("ConfirmBuyDogPopup").set_position(
#	get_parent().get_viewport_rect().size/2 
#	- get_node("ConfirmBuyDogPopup").get_rect().size/2)
	get_node("ConfirmBuyDogPopup").show()
	get_node("ConfirmBuyDogPopup").set_position(Vector2(120,220))
	dog_to_purchase = 1 if dog_type[0] == 'G' else (
	2 if dog_type[0] == 'A' else 3)
#	var text = "Confirm purchase of " + dog_type + "?"
	var text = dog_type
	get_node("ConfirmBuyDogPopup/Container/CenterContainer/Label").set_text("Confirm purchase of " + "\n" + dog_type + "?")
func _on_Cancel_button_down():
	get_node("BuyDogPopup").hide()
	get_node("CancelSound").play()


func _on_BuyGoldenBrown_button_down():
	buy_dog("Golden Brown")
	get_node("AcceptSound").play()


func _on_BuyApricotPoodle_button_down():
	buy_dog("Apricot Poodle")
	get_node("AcceptSound").play()


func _on_WhiteBichon_button_down():
	buy_dog("White Bichon")
	get_node("AcceptSound").play()


func _on_Yes_button_down():
	var cash = get_parent().cash
	if cash - 5000 >= 0:
		get_parent().cash -= 5000
		get_parent().purchase_dog(dog_to_purchase, 
					Vector2(250, 250))
		get_node("ConfirmBuyDogPopup").hide()
		get_node("AcceptSound").play()
	else:
		$error.play()


func _on_No_button_down():
	get_node("CancelSound").play()
	get_node("ConfirmBuyDogPopup").hide()


func _on_Credits_button_down():
	$PopupPanel.hide()
	var Credits = load("res://Credits.tscn")
	var credits = Credits.instance()
	credits.set_position(get_viewport_rect().size*Vector2(0.01, 0.25))
	get_parent().add_child(credits)
