extends Camera2D

# Camera is disabled and should not affect gameplay.
var prev_mouse = null
var pause = false

func _process(delta):
	if Input.is_mouse_button_pressed(BUTTON_LEFT) and not pause:
		if prev_mouse == null:
			prev_mouse = get_parent().get_viewport().get_mouse_position()
		var curr = get_parent().get_viewport().get_mouse_position()
		var move = - curr + prev_mouse
		set_position(move + get_position())
		prev_mouse = curr
	else:
		prev_mouse = null
	pause = false


func _input(event):
	if event is InputEventMouse:
		if event.is_pressed():
			if event.button_index == BUTTON_WHEEL_UP:
				zoom *= 0.95
			elif event.button_index == BUTTON_WHEEL_DOWN:
				zoom *= 1.05
